# How to Optimize Loops
# 如何优化循环

Sometimes you'll encounter loops, or recursive functions, that take a long time to execute and are bottlenecks in your product. Before you try to make the loop a little faster, spend a few minutes considering if there is a way to remove it entirely. 

有时候你会遇到一些需要很长时间来运行的循环或者递归函数，这些都是你产品发展的阻碍。在你尝试使这个循环快一点之前，花一点时间去考虑是否有一种方法可以彻底的解决它。

Would a different algorithm do? Could you compute that while computing something else? If you can't find a way around it, then you can optimize the loop. This is simple; move stuff out. 

用不同的算法可以做到吗？  	你能同时计算两种不同的事情吗？如果你不能找到这种方法，那么你可以优化这个循环。						  这是简单的：将所有的的资料移出。

In the end, this will require not only ingenuity but also an understanding of the expense of each kind of statement and expression. Here are some suggestions:

最后，这需要的将不仅仅是智慧还要能够理解每个类型的语句和表达式。这里有一些建议：

- Remove floating point operations.

- 删除浮点运算

- Don't allocate new memory blocks unnecessarily.

- 不要无谓地分配新的内存块

- Fold constants together.

- 常量折叠

- Move I/O into a buffer.

- 将I/O移动到同一个缓冲区

- Try not to divide.

- 不要尝试划分

- Try not to do expensive typecasts.

- 尽量不要做昂贵的类型转换

- Move a pointer rather than recomputing indices.

- 移动指针而不是验算指标

The cost of each of these operations depends on your specific system. On some systems compilers and hardware do these things for you. Clear, efficient code is better than code that requires an understanding of a particular platform.

每一个操作的成本取决于你的特定的系统。	在一些系统编译器和硬件为你做这些事情。明显地，高效的代码要比需要一个特定平台的代码好。

Next [How to Deal with I/O Expense](08-How to Deal with IO Expense.md)

下一篇 [如何处理I/O损耗](08-How to Deal with IO Expense.md)