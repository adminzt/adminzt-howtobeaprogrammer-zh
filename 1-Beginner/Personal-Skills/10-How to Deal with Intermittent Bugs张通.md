﻿# How to Deal with Intermittent Bugs
# 如何处理间歇性错误
The intermittent bug is a cousin of the 50-foot-invisible-scorpion-from-outer-space kind of bug. This nightmare occurs so rarely that it is hard to observe, yet often enough that it can't be ignored. You can't debug because you can't find it.

间歇性错误是在外太空中看不见的一种50英尺的蝎子的表弟。						这种噩梦很少发生，所以也很难观察，但这也是经常不能被忽视的。						你不能调试，因为你找不到它。

Although after 8 hours you will start to doubt it, the intermittent bug has to obey the same laws of logic everything else does. What makes it hard is that it occurs only under unknown conditions. Try to record the circumstances under which the bug does occur, so that you can guess at what the variability really is. The condition may be related to data values, such as ‘This only happens when we enter *Wyoming* as a value.’ If that is not the source of variability, the next suspect should be improperly synchronized concurrency.

虽然8小时后你会开始产生怀疑，间歇性错误必须遵循其他事物也遵守的相同的逻辑。							因为间歇性错误只发生在未知的情况下，所以解决它是很困难的。尝试着记录下当错误发生时的情况，						这样你就可以猜测一下真正发生变化的是什么。		这个条件可能与数据的值相关，例如“这只发生在当我们进入‘怀饿明’作为值。”					如果这不是可变性的来源，则下一个应该怀疑的是不正确的同步并发。

Try, try, try to reproduce the bug in a controlled way. If you can't reproduce it, set a trap for it by building a logging system, a special one if you have to, that can log what you guess you need when it really does occur. Resign yourself to that if the bug only occurs in production and not at your whim, this may be a long process. The hints that you get from the log may not provide the solution but may give you enough information to improve the logging. The improved logging system may take a long time to be put into production. Then, you have to wait for the bug to reoccur to get more information. This cycle can go on for some time.

尝试，尝试，尝试重现错误控制的方式。			如果你不能重现它，那么你可以通过建立一个记录系统，如果必要的话你可以建立一个特殊的记录系统，当间歇性错误确实发生的时候它可以在你需要的时候记录下你的猜测。		排除自己的原因，如果这个错误仅仅发生在在它生产的过程中而不是因为你的一时兴起，这可能是一个很漫长的过程。	你从你的记录日志中得到的提示信息可能不会提供解决方案，但是可以提供给你足够的信息来改善你的日志记录。				改进后的日志系统可能需要很长时间才能投入生产。				  然后，你必须等待错误重演，以此来获得更多的信息。			这个周期可以持续一段时间。

The stupidest intermittent bug I ever created was in a multi-threaded implementation of a functional programming language for a class project. I had very carefully ensured correct concurrent evaluation of the functional program, good utilization of all the CPUs available (eight, in this case). I simply forgot to synchronize the garbage collector. The system could run a long time, often finishing whatever task I began, before anything noticeable went wrong. I'm ashamed to admit I had begun to question the hardware before my mistake dawned on me.

我所创造的最愚蠢的错误是一个多线程实现的函数式编程语言类项目。											我非常小心地确保并发评价功能程序的正确性，					很好的利用所有可用的CPU（八，在这种情况下）。我只是忘记同步垃圾回收器。								这个系统可以运行很长的一段时间，在发现任何的明显错误之前，我开始经常使用它完成任务。				我羞于承认我在意识到自己错误之前就已经开始对硬件进行查询了。

At work we recently had an intermittent bug that took us several weeks to find. We have multi-threaded application servers in Java™ behind Apache™ web servers. To maintain fast page turns, we do all I/O in small set of four separate threads that are different than the page-turning threads. Every once in a while these would apparently get ‘stuck’ and cease doing anything useful, so far as our logging allowed us to tell, for hours. Since we had four threads, this was not in itself a giant problem - unless all four got stuck. Then the queues emptied by these threads would quickly fill up all available memory and crash our server. It took us about a week to figure this much out, and we still didn't know what caused it, when it would happen, or even what the threads where doing when they got ‘stuck’.

在最近的工作中，我们有一个间歇性的错误，我们花了好几个星期的时间去寻找。	我们的多线程应用程序服务器应用于Apache Web服务器的Java™™。			为了保持快速翻页，我们在四个小的单独线程中做了全部的 输入/输出 ，这些不同于翻页线程。							   每隔一段时间这些程序都会有明显的卡顿，而且在我们的日志系统告诉我们这些之前它还会停止做任何有用的事情。数小时后				自从我们有了这四个线程，在理论上来说这不是个大问题，除非四个线程全都卡住了。然后，这些被线程清空的队列会迅速填满所有可用的内存并且使我们的服务器崩溃。								我们花了大约一个星期的时间去计算这件事，并且我们仍然不知道是什么原因导致线程卡顿，当它发生的时候，甚至是当线程在做什么的时候会卡住。

This illustrates some risk associated with third-party software. We were using a licensed piece of code that removed HTML tags from text. Due to its place of origin we affectionately referred to this as ‘the French stripper‘. Although we had the source code (thank goodness!) we had not studied it carefully until by turning up the logging on our servers we finally realized that the email threads were getting stuck in the French stripper.

这说明了一些与第三方软件相关的风险。				我们用了许可证代码，从文本中删除了HTML标签。				由于其产地，我们亲切地称这是“法国汽提器”						虽然我们有源代码（谢天谢地！）但是我们没有仔细研究过它，直到我们通过查看我们系统的记录，我们最终意识到我们的电子邮件线程被困在了“法国法国汽提器”里。

The stripper performed well except on some long and unusual kinds of texts. On these texts, the code was quadratic or worse. This means that the processing time was proportional to the square of the length of the text. Had these texts occurred commonly, we would have found the bug right away. If they had never occurred at all, we would never have had a problem. As it happens, it took us weeks to finally understand and resolve the problem.

除了一些比较长的或者一些不是寻常类型的文本外，汽提器都执行的很好。	在这类文本中，代码是二次或者更复杂的。这就意味着系统处理时间与文本长度的平方成正比。								通常这样类型的文本都会发生问题，我们也会立刻发现这个错误的。		如果它们从来没有发生过，我们将永远不会有问题。					当它发生的时候，那将花费我们几个星期的时间才能最终理解和解决这个问题。

Next [How to Learn Design Skills](11-How to Learn Design Skills.md)

下一篇[如何学习设计技巧](11-How to Learn Design Skills.md)