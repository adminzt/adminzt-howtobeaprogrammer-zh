# How to Learn Design Skills

# 如何学习设计技巧

To learn how to design software, study the action of a mentor by being physically present when they are designing. Then study well-written pieces of software. After that, you can read some books on the latest design techniques.

学习如何设计软件，当它们被设计的时候，通过实际操作来学习指导经验。						他们研究学习编写良好的软件。			在那之后，你可以去阅读一些书上最新的设计技术。

Then you must do it yourself. Start with a small project. When you are finally done, consider how the design failed or succeeded and how you diverged from your original conception. Then move on to larger projects, hopefully in conjunction with other people. Design is a matter of judgement that takes years to acquire. A smart programmer can learn the basics adequately in two months and can improve from there.

然后你必须亲自去做。从一个小的项目开始。		当你最终完成之后，可以考虑一下你的设计与预想的设计理念相比有何不同，是成功了还是失败了。				然后你就可以开始着手一些大项目了，充满希望的和其他人相互协调。			设计是一个需要花费数年才能获得的判断。				一个聪明的程序员能够在两个月内充分的学习基础知识，并且能够从中有所提高。

It is natural and helpful to develop your own style, but remember that design is an art, not a science. People who write books on the subject have a vested interest in making it seem scientific. Don't become dogmatic about particular design styles.

发展自己的风格是很自然地，并且这也有助于发展自己的风格，但是你需要记住设计是一门艺术，而不是一门科学。那些在这个项目上写书的人，有一种既得的兴趣，那便是使这些看起来更具科学性。		不要被一种特定的设计风格教条约束。

Next [How to Conduct Experiments](12-How to Conduct Experiments.md)

下一篇[如何进行试验](12-How to Conduct Experiments.md)