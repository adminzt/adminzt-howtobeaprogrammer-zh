﻿# How to Conduct Experiments

# 如何进行试验

The late, great Edsger Dijkstra has eloquently explained that Computer Science is not an experimental science[ExpCS] and doesn't depend on electronic computers. As he puts it referring to the 1960s [Knife],

已故的伟人艾兹格·迪科斯彻曾经非常富有表现力地说明计算机科学不是一门实验科学[ExpCS]，并且不依赖于电子计算机。正如他在20世纪60年代中提出的[手术刀]。

> ...the harm was done: the topic became known as “computer science” - which, actually, is like referring to surgery as “knife science” - and it was firmly implanted in people's minds that computing science is about machines and their peripheral equipment.

>......这个危害被解决了：这个课题被称作是“计算机科学”，实际上，很像是外科技术中的“手术刀科学”						计算机科学是有关于机器及其相关的设备的观念被牢牢的植入在人们的头脑中。 

Programming ought not to be an experimental science, but most working programmers do not have the luxury of engaging in what Dijkstra means by computing science. We must work in the realm of experimentation, just as some, but not all, physicists do. If thirty years from now programming can be performed without experimentation, it will be a great accomplishment of Computer Science.

编程不应该是一种试验科学，				但是大多数的程序员都没有通过计算机科学从事过这种奢侈的Dijkstra算法。					我们必须在试验领域中工作，正如一些物理学家们一样，但并不是全部的物理科学家都一样。	如果从现在开始三十年内开始编程，最终可以执行但没有进行过试验，那将是计算机科学的一个伟大成就。

The kinds of experiments you will have to perform include:

你将要执行的实验种类包括：

- Testing systems with small examples to verify that they conform to the documentation or to understand their response when there is no documentation,

- 测试系统的小例子来验证他们是否符合文档或者了解一下当它们没有文档时的反应。

- Testing small code changes to see if they actually fix a bug,

- 测试小代码更改来看看是否能够实际修复错误。

- Measuring the performance of a system under two different conditions due to imperfect knowledge of there performance characteristics,

- 在不同的情况下测量系统的性能，由于性能特点的不完美理论知识认知。

- Checking the integrity of data, and

- 检查数据的完整性。

- Collecting statistics that may hint at the solution to difficult or hard-to-repeat bugs.

- 收集可能解决问题或者难以重复错误的线索。

I don't think in this essay I can explain the design of experiments; you will have to study and practice. However, I can offer two bits of advice.

我不认为在这篇文章中我能够解释清楚这个实验的设计；所以你将必须学习并且进行实践。				然而，我可以提供两个建议。

First, try to be very clear about your hypothesis, or the assertion that you are trying to test. It also helps to write the hypothesis down, especially if you find yourself confused or are working with others.

首先，尝试着非常清楚地了解你的假设，或者明确声明你正在进行尝试。					这也有助于你将你的假设写下来，特别是如果你发现自己的困惑或者在与其他人合作时。

You will often find yourself having to design a series of experiments, each of which is based on the knowledge gained from the last experiment. Therefore, you should design your experiments to provide the most information possible. Unfortunately, this is in tension with keeping each experiment simple - you will have to develop this judgement through experience.

你将会经常发现你自己不得不设计的一系列实验，它们中的每一个都是以上一个实验所获得的知识为基础。							因此，你应该设计你的实验用来提供最有可能的信息。					不幸的是，这是一个保持每个实验简单的紧张局势——你必须通过经验来发展这个判断。

Next [Team Skills - Why Estimation is Important](../Team-Skills/01-Why Estimation is Important.md)

下一篇[团队技能-为什么估计是重要的](../Team-Skills/01-Why Estimation is Important.md)