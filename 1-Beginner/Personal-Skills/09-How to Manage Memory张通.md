# How to Manage Memory
# 如何管理内存
Memory is a precious resource that you can't afford to run out of. You can ignore it for a while but eventually you will have to decide how to manage memory.

内存是一种你无法负担得起的珍贵资源。你可以选择暂时的忽略它，但是最后你将不得不决定去学习如何管理内存。

Space that needs to persist beyond the scope of a single subroutine is often called *heap allocated*. A chunk of memory is useless, hence *garbage*, when nothing refers to it. Depending on the system you use, you may have to explicitly deallocate memory yourself when it is about to become garbage. More often you may be able to use a system that provides a *garbage collector*. A garbage collector notices garbage and frees its space without any action required by the programmer. Garbage collection is wonderful: it lessens errors and increases code brevity and concision cheaply. Use it when you can.

空间中持续超出单个子程序的范围通常被称为“堆分配”。当没有提到内存块的时候，它就是没用的，因此有点像“垃圾”。									根据你所使用的系统，在它产生垃圾的时候你可能不得不显示释放一些内存。							   在大多数情况下，你所使用的系统都会提供一个“垃圾回收器”。			  垃圾回收器关注着电脑里的垃圾并释放它所占用的空间，而不需要程序员的任何行动指令。			垃圾回收器是非常好的：它不仅能够减少错误和增加代码的简洁性而且整体来说简洁便宜。当你有条件使用它的时候不要犹豫，直接使用吧。

But even with garbage collection, you can fill up all memory with garbage. A classic mistake is to use a hash table as a cache and forget to remove the references in the hash table. Since the reference remains, the referent is non-collectable but useless. This is called a *memory leak*. You should look for and fix memory leaks early. If you have long running systems memory may never be exhausted in testing but will be exhausted by the user.

但即便你有垃圾回收器，你也可以用垃圾填满你的内存。			一个典型的错误是使用哈希表进行缓存并且忘记在哈希表中删除引用。							自此这个参照保留下来，这个参照是没有收藏价值的，但是没有用处。这种情况被称作“内存泄露”。你应该尽可能早地查找并且修复内存泄露。如果你有长时间运行的系统内存测试，这可能永远不会疲惫，但是它将会被用户耗尽。

The creation of new objects is moderately expensive on any system. Memory allocated directly in the local variables of a subroutine, however, is usually cheap because the policy for freeing it can be very simple. You should avoid unnecessary object creation.

在任何一个系统中创建一个新对象是中等价值的。			在本地变量中直接分配的一个子程序，但是，通常是很便宜的，那是因为释放它的政策是非常简单的。								你应该避免创建不必要的对象。

An important case occurs when you can define an upper bound on the number of objects you will need at one time. If these objects all take up the same amount of memory, you may be able to allocate a single block of memory, or a buffer, to hold them all. The objects you need can be allocated and released inside this buffer in a set rotation pattern, so it is sometimes called a ring buffer. This is usually faster than heap allocation.

一个重要的情况发生在当你需要的时候你可以定义一个有数量上限的对象。						如果这些对象全都占有相同的内存，你可以分配一个单独的内存块，或者是一个缓冲区，以保持它们的数据完整。						你需要的对象可以在一组旋转模式中分配和释放该缓冲区，因此有时候它又被称为环缓冲区。这种方式通常比堆分配快。

Sometimes you have to explicitly free allocated space so it can be reallocated rather than rely on garbage collection. Then you must apply careful intelligence to each chunk of allocated memory and design a way for it to be deallocated at the appropriate time. The method may differ for each kind of object you create. You must make sure that every execution of a memory allocating operation is matched by a memory deallocating operation eventually. This is so difficult that programmers often simply implement a rudimentary form or garbage collection, such as reference counting, to do this for them.

有时候你必须显示释放已经分配了的空间，这样才能对空间进行重新分配，而不是依靠垃圾回收器来处理空间。			然后你必须小心的将情报应用于每个块分配的内存和设计一种可以在适当时候将其收回的方式。								这个方法可能不同与你所创建的每一个对象。		你必须确保每一个执行的内存分配操作最终都能匹配一个内存回收操作。								为他们做这些是如此的困难，程序员通常只是简单地实现一个基本的形式或者是垃圾收集，比如引用计数。

Next [How to Deal with Intermittent Bugs](10-How to Deal with Intermittent Bugs.md)

下一篇[如何处理间歇性错误](10-How to Deal with Intermittent Bugs.md)