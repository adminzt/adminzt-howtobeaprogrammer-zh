# How to Deal with I/O Expense
# 如何处理 I/O 损耗
For a lot of problems, processors are fast compared to the cost of communicating with a hardware device. This cost is usually abbreviated I/O, and can include network cost, disk I/O, database queries, file I/O, and other use of some hardware not very close to the processor. Therefore building a fast system is often more a question of improving I/O than improving the code in some tight loop, or even improving an algorithm.

对于很多问题来讲，与硬件通信设备的通信成本相比，处理器的速度要快。					这个成本通常包括压缩的输入/输出，并且包括网络成本，磁盘输入/输出，数据查询，文件输入/输出，和其他一些硬件的使用，不是很接近处理器。					   因此构建一个高效的系统通常是一个改进输入/输出的问题，而不是改善一些紧密循环中的代码，甚至改进算法。

There are two very fundamental techniques to improving I/O: caching and representation. Caching is avoiding I/O (generally avoiding the reading of some abstract value) by storing a copy of that value locally so no I/O is performed to get the value. The first key to caching is to make it crystal clear which data is the master and which are copies. There is only one master - period. Caching brings with it the danger that the copy sometimes can't reflect changes to the master instantaneously.

这里有两个非常基本的技术来提高 输入/输出 ：缓存和表示。					缓存是避免输入/输出（通常是避免一些抽象值的读取），通过将该值的副本存储在本地，所以不执行任何输入/输出来获取该值。						缓存的第一个关键是使它清楚哪些数据是主数据，哪些是副本。只有一个主周期。								缓存带来的危险是副本有时候不能及时的反应来自主机的变化。

Representation is the approach of making I/O cheaper by representing data more efficiently. This is often in tension with other demands, like human readability and portability.

表示是一种使输入/输出更便宜，更有效地表示数据的方法。					   这些经常和其他的要求，例如人的可读性和可移植性，紧密相连。

Representations can often be improved by a factor of two or three from their first implementation. Techniques for doing this include using a binary representation instead of one that is human readable, transmitting a dictionary of symbols along with the data so that long symbols don't have to be encoded, and, at the extreme, things like Huffman encoding.

表示通常可以提高两到三倍于他们的第一个实现。							这样的技术包括使用二进制表示而不是一种具有人类可读性的表示方法，发送一个字典符号并伴随着数据，这样的话长的符号就不需要做出编码了，而且，在极端情况下，比如哈夫曼编码。

A third technique that is sometimes possible is to improve the locality of reference by pushing the computation closer to the data. For instance, if you are reading some data from a database and computing something simple from it, such as a summation, try to get the database server to do it for you. 

第三个技巧是有时通过推动计算更接近的数据来尽可能的提高本地的引用。例如，如果你正在从数据库中读取一些数据，并且用它进行一些简单的运算，例如求一些数的总合，试图让数据库服务器为您做到这些。

This is highly dependent on the kind of system you're working with, but you should explore it.

这高度依赖于你所使用的系统，但你应该探索它。

Next [How to Manage Memory](09-How to Manage Memory.md)

下一篇[如何管理内存](09-How to Manage Memory.md)
