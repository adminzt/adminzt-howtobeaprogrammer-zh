# 如何明智的阅读文档

人生苦短,写垃圾没人会读,如果你写废话,没人会阅读它。因此一个好的文档是最好的。经理常常不理解这一点,因为即使是坏的文档给他们一种虚假的安全感,他们不依赖于程序员。如果有人绝对坚持认为你写真正无用的文档,悄悄地说“是的”,开始寻找一份更好的工作。

没有什么很有效的把一个准确的估计需要的时间产生良好的文档到估计放松的需求文档。事实是冷和硬:文档,喜欢测试,可以多次超过开发代码。

写好文档,首先,良好的写作。我建议你找书写作,研究和实践。但是,即使你是一个差劲的作家或差命令的语言你必须文档,黄金法则是您真正需要的所有东西:“己所不欲做给你们。“花时间认真考虑谁将阅读你的文档,他们需要什么,以及如何教他们。如果你这样做,你将是一个高于平均水平的文档的作家,和一个优秀的程序员。

时记录代码本身,而不是生产的文档可以被非程序员阅读,我所知的最好的程序员普遍情绪:只写的代码和文档代码的地方你不能弄清楚通过编写代码本身。有两个原因。首先,人需要看到代码级文档在大多数情况下能够和愿意阅读代码。诚然,这似乎更有经验的程序员比初学者。然而,更重要的是,代码和文档不能一致,如果没有文档。源代码可以在最坏的情况是错误的和困惑。的文档,如果没有写完全,可以说谎,这是糟糕一千倍。

这并不是负责任的程序员更容易。如何编写的代码吗?,即使是什么意思?它的意思是:

编写代码知道有人会阅读它;

应用黄金法则;

-选择一个解决方案很简单,即使你可以与另一个解决方案更快;

-牺牲小混淆代码的优化;

-考虑读者和支出你的一些宝贵的时间,让它更容易在她;和

-不会使用一个函数名像foo,酒吧,或doIt !

下一篇 [如何处理不好的代码](06-How to Work with Poor Code.md)